These are instructions on how to configure a Raspberry Pi for various projects.

# Requirements

You will need at least:
- Raspberry Pi (Zero W recommended)
- 4GB MicroSD Card (64GB recommended)
- [Latest Raspbian Lite](https://www.raspberrypi.org/downloads/raspbian/)
- [Etcher](https://etcher.io/)

# Installation

Use Etcher to burn the Raspbian Lite to the SD card.

Put the card in the Pi and connect keyboard, HDMI, ethernet (if available) and power.

> *Note:* The default username and password are `pi` and `raspberry`.

Use `raspi-config` to:

1. *Locale Options* 
    1. Update all 4 sections (keyboard should be `en.US-UTF-8`. Don't worry if it complains, just keep going.)
2. *Interface Options*
    1. Enable SSH
3. *Networking Options*
    1. Configure Wifi
    2. Update Hostname (use something that identifies what this Pi does. I will use `SensorPi`.)
4. Change the default user password

I recommend doing them in that order because some characters are different on an American keyboard and the default *Locale Options* can mess up your passwords.

    sudo raspi-config

Exit the tool



Get the IP

    hostname -I

You now have a "headless" Raspberry Pi. You no longer need a monitor or keyboard.

SSH into the Pi from another machine and complete the remaining steps.

----

# One Liner

Run this to download and execute the `install.sh`.

    bash <(curl -s https://gitlab.com/the4tress/SensorPi/raw/master/install.sh)

# Continued

After you SSH in from another machine, continue with these steps

# Install Basics

Get updates and install basics

    sudo apt-get update
    sudo apt-get upgrade -y

Install basic tools

    sudo apt-get install -y htop iftop tmux ncdu vim git nginx locate build-essential


## Configure Nginx

Clone my repo with `nginx` configs, update and copy them, then enable `nginx`.

    cd ~
    git clone https://gitlab.com/the4tress/SensorPi.git
    sed -i -- "s/\$HOSTNAME/$HOSTNAME/g" SensorPi/configs/nginx/nginx_proxy.conf
    sudo cp SensorPi/configs/nginx/nginx_proxy.conf /etc/nginx/conf.d/sensor.conf
    sudo rm /etc/nginx/sites-enabled/default
    sudo systemctl enable nginx

> *Note:* Your `nginx` configs are now located in `/etc/nginx/conf.d/sensor.conf`. Remember that if you need to change them based on what you install later in these instructions.



## Install Node-RED

    bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)

Enable and start, then stop `node-red`, update configs, then start `node-red` again

    sudo systemctl enable nodered
    sudo systemctl start nodered
    echo "# Waiting for Node-Red to boot for the first time. Waiting 60 seconds..."
    sleep 60
    sudo systemctl stop nodered
    cp ~/SensorPi/configs/node-red/settings.js ~/.node-red/settings.js
    sudo systemctl start nodered

`node-red` is now running


## Install Cloud9

> *Note:* This step is optional depending on the project. You will need to update the `sensor.conf` file for `nginx` to enable this as the default path.

    cd ~
    git clone https://github.com/c9/core.git .c9sdk
    cd .c9sdk
    scripts/install-sdk.sh
    cd ~

Install `pm2`, enable `startup`, start `c9`, then save the config for startup.

    npm install -g pm2
    pm2 startup
    pm2 start ~/.c9sdk/server.js --name="c9" -- -w ~/
    pm2 save

## Reboot

    sudo reboot

Now you can get to:

- `node-red` at http://SensorPi/node-red
- `node-red` endpoints at http://SensorPi/api
- `c9` at http://SensorPi/
