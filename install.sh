if [ $# -eq 0 ]
  then
    echo "No Slack Webhook supplied."
    echo "Usage: install.sh <SLACK_WEBHOOK>"
    exit 1
fi

SLACK_WEBHOOK=$1
echo "export SLACK_WEBHOOK=${SLACK_WEBHOOK} >> ~/.bashrc"

MY_IP=$(ip addr show wlan0 | grep -Po 'inet \K[\d.]+')

# Notify starting
DATA="Started installing <https://gitlab.com/the4tress/SensorPi|SensorPi> on ${MY_IP}"
HEADER="Content-type: application/json"
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}


# Install the basics
DATA="${MY_IP} Installing the basics... :hourglass_flowing_sand:"
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y htop iftop tmux ncdu vim git nginx locate build-essential python-rpi.gpio expect


# Grab the repo
DATA="${MY_IP} Cloning repo..."
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

cd ~
git clone https://gitlab.com/the4tress/SensorPi.git
sed -i -- "s/\$HOSTNAME/$HOSTNAME/g" SensorPi/configs/nginx/nginx_proxy.conf
sudo cp SensorPi/configs/nginx/nginx_proxy.conf /etc/nginx/conf.d/sensor.conf
sudo rm /etc/nginx/sites-enabled/default
sudo systemctl enable nginx
chmod +x SensorPi/startup.sh
(crontab -l 2>/dev/null; echo "@reboot ${HOME}/SensorPi/startup.sh ${SLACK_WEBHOOK}") | crontab -


# Install Node-RED
DATA="${MY_IP} Installing \`nodejs\` and \`node-red\`... :hourglass_flowing_sand:\n\n>:warning:There will be 2 prompts here. Select *Yes* for both."
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
mkdir .node-red
cp ~/SensorPi/configs/node-red/settings.js ~/.node-red/settings.js
sudo systemctl enable nodered
sudo systemctl start nodered

# Install c9
DATA="${MY_IP} Installing \`c9\`... :hourglass_flowing_sand:"
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

cd ~
git clone https://github.com/c9/core.git .c9sdk
cd .c9sdk
scripts/install-sdk.sh
cd ~


# Install pyenv
DATA="${MY_IP} Installing \`pyenv\`..."
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
echo 'export PATH="~/.pyenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc


# Install pm2
DATA="${MY_IP} Installing \`pm2\`"
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

sudo npm install -g pm2
pm2 startup
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u ${USER} --hp ${HOME}


# Start c9
DATA="<http://${MY_IP}:8181|${MY_IP} Starting \`c9\`"
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

pm2 start ~/.c9sdk/server.js --name="c9" -- -w ${HOME}
pm2 save


# Notify completion
DATA="Finished installing <https://gitlab.com/the4tress/SensorPi|SensorPi> on ${MY_IP}\n\nRebooting for the first time...\n\nWhen finished rebooting you can access \`c9\` at <http://${MY_IP}|http://${MY_IP}/> and Node-RED at <http://${MY_IP}/node-red|http://${MY_IP}/node-red/>."
HEADER="Content-type: application/json"
curl -X POST -H ${HEADER} --data "{\"text\":\"${DATA}\"}" ${SLACK_WEBHOOK}

# Reboot
sudo reboot