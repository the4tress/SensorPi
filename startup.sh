#!/bin/bash

MY_IP=$(ip a | sed -En "s/.*inet\s//p" | sed -En "s/\/.*//p" | grep -v "127.0.0.1")
DATA="{\"text\":\"${MY_IP} just booted.\"}"

curl -X POST -H 'Content-type: application/json' --data "${DATA}" $1