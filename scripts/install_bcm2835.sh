#!/bin/bash
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" 
    exit 1
fi

wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.58.tar.gz
tar -zxvf bcm2835*.gz
cd bcm2835*/
./configure
make
make check
make install
cd ..
rm -rf bcm2835*